import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

const coordMap = {
  0: {row: 0, col: 0},
  1: {row: 0, col: 1},
  2: {row: 0, col: 2},
  3: {row: 1, col: 0},
  4: {row: 1, col: 1},
  5: {row: 1, col: 2},
  6: {row: 2, col: 0},
  7: {row: 2, col: 1},
  8: {row: 2, col: 2},
};

function Square(props) {
  return (
    <button 
      className={`square ${props.isHighlighted ? 'square-highlighted' : ''}`} 
      onClick={props.onClick}>
      {props.value}
    </button>
  );
}

class Board extends React.Component {
  renderSquare(i) {
    return (
      <Square   
        value={this.props.squares[i]}
        onClick={() => this.props.onClick(i)}
        key={i}
        isHighlighted={this.props.highlightedSquares.includes(i)}
      />
    );
  }

  render() {
    return (
      <div>
        { 
          [0, 1, 2].map(row => 
            (
              <div className="board-row" key={row}>
                { [0, 1, 2].map(col => this.renderSquare(3 * row + col)) }
              </div>
            )
          )
        }
      </div>
    );
  }
}

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [{
        squares: Array(9).fill(null),
        squareNum: null 
      }],
      xIsNext: true,
      stepNumber: 0
    }
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = [...current.squares];
    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = this.state.xIsNext ? 'X' : 'O';
    this.setState({
      history:  [ ...history, {squares, squareNum: i} ],
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
    });
  }

  jumpTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0,
    });
  } 

  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const winner = calculateWinner(current.squares);

    let nextPlayer;
    if (winner) {
      nextPlayer = `Winner: ${winner.sign}`;
    } else if (this.state.stepNumber === 9) {
      nextPlayer = 'Draw';
    } else {
      nextPlayer = `Next player: ${this.state.xIsNext ? 'X' : 'O'}`;
    }

    return (
      <div className="game">
        <div className="game-board">
          <Board 
            squares={current.squares} 
            highlightedSquares={ winner ? winner.squares : [] } 
            onClick={(i) => this.handleClick(i)}/>
        </div>
        <div className="game-info">
          <div className='status'>{nextPlayer}</div>
          <MovesHistory 
            history={this.state.history}
            activeMove={this.state.stepNumber}
            jumpTo={(step) => this.jumpTo(step)}
          />
        </div>
      </div>
    );
  }
}

class MovesHistory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sortMovesAsc: true
    }
  }

  reverseOrder() {
    this.setState({
      sortMovesAsc: !this.state.sortMovesAsc
    });
  }

  render() {
    const moves = this.props.history.map((snapshot, move) => {
      var coords = '';
      if (snapshot.squareNum !== null) {
        const {row, col} = coordMap[snapshot.squareNum];
        coords = `(${row}, ${col})`;
      }
  
      const desc = move ? 'Go to move #' + move : 'Go to game start';
  
      return (
        <li key={move} >
          <button 
            className={move === this.props.activeMove ? 'move-active' : ''} 
            onClick={() => this.props.jumpTo(move)}>{desc} {coords}</button>
        </li>
      );
    });
  
    return (
      <div>
        <button onClick={() => this.reverseOrder()}>⇅</button>
        <ul className='moves-list'>{this.state.sortMovesAsc ? moves : moves.reverse()}</ul>
      </div>
    );
  }
}

// ========================================

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return { sign: squares[a], squares: [a, b, c] };
    }
  }
  return null;
}